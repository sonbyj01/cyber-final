#!/usr/bin/env python3

from scapy.all import *
from pathlib import Path
import pandas as pd
import re

def process_dns(packet):
    query = pd.DataFrame()

    try:
        query = pd.DataFrame(['DNS',
                              packet['Ethernet'].src,
                              packet['IP'].src,
                              packet['DNS'].qd.qname.decode('utf-8')]).T
    except:
        pass

    return query

def process_http(packet):
    query = pd.DataFrame()

    try: 
        request = re.search('(.*)\r\n', packet['Raw'].load.decode()).group(1)
        host = re.search('.*Host: (.*)\r\n', packet['Raw'].load.decode()).group(1)

        if packet['Raw'].load == '':
            pass
        else:
            query = pd.DataFrame(['HTTP',
                                packet['Ethernet'].src,
                                packet['IP'].src,
                                request + ' ' + host]).T
    except:
        pass
    
    return query

def process_packet(packet):
    if re.findall('.*DNS Qry.*', packet.summary()):
        pass
        c = process_dns(packet)
        if not c.empty:
            print(c)
    elif re.findall('.*http .*', packet.summary()):
        c = process_http(packet)
        if not c.empty:
            print(c)

def main():
    pcap_file_name = input("Enter the .pcap file name: ")
    path = Path.joinpath(Path().cwd(), pcap_file_name)

    pcap_packets = sniff(offline=str(path), prn=process_packet)

main()
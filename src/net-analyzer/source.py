#!/usr/bin/env python3

from scapy.all import *
from pathlib import Path 
import base64
import re

conf.iface = "ens33"

# def sniffData(pkt):
#     if pkt.haslayer( Raw ): 
#         header = pkt.getlayer( Raw ).load

#         if header.startswith('GET'):
#             if '/search?' in header:
#                 q = re.search(r'q=(.*)\w', header)

#                 if q:
#                     src = pkt.getlayer(IP).src
#                     query = q.group().split(' ')[0].lstrip('q=')
#                     print("{} searched for {}".format(src, query))

def packet_analysis(packet_type, packet):
    if packet_type == 'DNS':
        dns_query = ''
        dns_id = -1
        try:
            #print(packet[0].getlayer(IP).src)
            dns_query = packet[packet_type].qd.qname.decode('utf-8')
            dns_id = int(packet[packet_type].id)
        except:
            pass
        return dns_query, dns_id
    # else:
    #     try:
    #         print(packet['HTTP'])
    #     except:
    #         pass


def analysis_pcap(pcap_file_name):
    # pcap_file = sniff(offline='{}'.format(pcap_file_name))
    # print(type(pcap_file))

    pcap_file_2 = rdpcap('{}'.format(pcap_file_name))

    id_name = -1
    for packet in pcap_file_2:
        dns_query, dns_id = packet_analysis('DNS', packet)
        packet_analysis('HTTP', packet)

        if dns_id == id_name:
            continue
        elif dns_id != -1:
            print(dns_query[:-1])
            id_name = dns_id

def main():
    #print(Path().cwd())
    # pcap_file_name = input("Enter your value: ")
    #pcap_file_name = 'sample_minecraft.pcap'
    pcap_file_name = 'http_request.pcap'
    #print(Path.joinpath(Path().cwd(), pcap_file_name))
    
    analysis_pcap(Path.joinpath(Path().cwd(), pcap_file_name))
    # sniff(prn=sniffData) # sends data one at a time

if __name__ == "__main__":
    main()
# Docker-Deployable HTTP/SSH Honeypots

## Dependencies
- python3
    - scapy
    - pandas
- python3-venv (optional)
- docker engine
    - https://docs.docker.com/engine/install/
- docker compose
    - https://docs.docker.com/compose/install/

## Usage
Deploying either honeypots {ssh | http}:
```bash
$ cd docker/docker-{protocol}
$ docker-compose up -d
```

For example:
```bash
$ cd docker/docker-http
$ docker-compose up -d
```

Filtering a .pcap file for http/dns requests:
```bash
# optional but good practice
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install scapy pandas

# running the actual program
$ python src/net-analyzer/process.py 
# or
$ chmod +x src/net-analyzer/process.py
$ ./src/net-analyzer/process.py

# note: when the program asks for the pcap file, if you input a relative path, it will start from wherever you are currently executing it. 

# for example, if you run "$ python process.py" inside the net-analyzer directory, it will look for the pcap file RELATIVE to the net-analyzer directory.
```
